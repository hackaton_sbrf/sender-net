import yaml;

class Configuration(object):
    _instance = None
    _params = None
    _config_file_path = "./configuration.yml"
    @staticmethod
    def instance():
        if Configuration._instance is None:
            Configuration._instance = Configuration()
        return Configuration._instance;

    def __init__(self):
        with open(Configuration._config_file_path, 'r') as file:
            self._params = yaml.load(file)
        # todo validate params

    def get_ethereum_node_host(self):
        return self._params['ethereum']['node']['host'];

    def get_ethereum_smart_contact(self):
        return self._params['ethereum']['smart_contract'];

    def get_ethereum_account(self):
        return self._params['ethereum']['account'];

    def get_temporary_path(self):
        return self._params['temporary']['path'];

    def get_mount_path(self):
        return self._params['mount']['path'];

    def get_storage_path(self):
        return self._params['storage']['path']

    def get_files_path(self):
        return self._params['files']['path']
