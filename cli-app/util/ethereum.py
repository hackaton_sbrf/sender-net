import json
import web3

from web3 import Web3
from solc import compile_source
from web3.contract import ConciseContract

class Ethereum(object):
    w3 = None
    documentflow=None
    def __init__(self, host, account,):
       self.w3 = Web3(Web3.HTTPProvider(host))
       self.w3.eth.defaultAccount = self.w3.eth.accounts [account]
       self.documentflow=self.w3
       self.documentflow = self.w3.eth.contract(
        	address="0xb19A5c551698514f37EFD3B59075A94286A07C7A",
                abi=[{'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}], 'name': 'postRecievedFileAction', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}, {'name': '_recepient', 'type': 'address'}], 'name': 'postFileSendAction', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}], 'name': 'postReadFailedFileAction', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [], 'name': 'getAddress', 'outputs': [{'name': '', 'type': 'address'}], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}], 'name': 'postReadSuccessFileAction', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [], 'name': 'getPendingPassword', 'outputs': [{'name': '', 'type': 'string'}], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}], 'name': 'getPasswordSendAction', 'outputs': [{'name': '', 'type': 'string'}], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [], 'name': 'getPendingFile', 'outputs': [{'name': '', 'type': 'string'}], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}, {'constant': False, 'inputs': [{'name': '_filehash', 'type': 'string'}, {'name': '_password', 'type': 'string'}], 'name': 'postPasswordSendAction', 'outputs': [], 'payable': False, 'stateMutability': 'nonpayable', 'type': 'function'}]
	)
    def get_own_private_key(self):
        return """-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAyK+8fQC9aiDQpTpwpJCyXjnk9SDKkE/Nys3POPcGf64FZeRu
x70QBh1sdUD6P3yZYH4eJrm913wNZQMgxOlFX1PXxfZYT9h/USOsb61YjA0ZWvT/
iwqJd3UlVXiNZf++OwFXqkhjS7EqtamIVS6DHA8SYAoJcGw710DiYg9iGtx3iMjA
DICA+V6kzvNp/j6DB03Nix9kcu4OTRPq2iPOJwL4Mxwon2S8i8XW7HgQvlZd0sPc
gDSh1pvABjF4gjvEViRXgjg2ejcOwl3QR0zBIOGhuzBPFR5gkaD25ed6mEnhrkBH
nG7Bu0LqHrNXjIctnl2F8vT6MqkQ+kQGIUDEuwIDAQABAoIBAED+sVPmrnG1qP0q
HRsfBbWAmIdPFGuCfQrGdutjDYbCbFsgjURBFfR1CRA8eA+hoebfUZKHK1Wjho8F
YVKEngQOml4xt048tpcnr4wHnxrUjzanDDASvIyksu+oeFrlZc2kVKkAgrAb89Ib
0tyLkMjAlNnnxkDTvNms2OSbSNGbrIUihA8GfaMK8vdN/HBZUDgo/POcTGrNCp4r
1Z0j4IiOCRl4E4RwyAZ+4RMcILvR9W7H/NfjLiBAeXRP6ZNyl3oJv+GRsUor1UXt
I/oBQwsDE/L/0hbgJNQY7CQeG2N48XQ17PaoRmy9lUr+phPJHFjahuJTM0yz+8SB
hbEYMiECgYEA4/TNIVQoudsIjsymSn9S1sNCgSlCRDSw/5Y2McsfD3G9O4M6oOZy
1+WiLDmA0m8uLgngiJIio5pIq0uKMD4Ad5pZ3yflu+dcX+KyhnXZmZmfTk9cX2zG
tC6mUDRMV/jL8BdY8neAgYq7cwBrvyAeRtr97wsWw5ucLX2yZwRaWEsCgYEA4WAb
ZiiKw8h9QsPlndI+IPoO1Lo7yERlcg9tKGiOKaZk3w41utscXgSaqxK3tOViL6e5
/5x82wkkM9NZt76K8Opt1CKUijsVQlyO7vuveN25hxQfCl1nMyTg9TMwZi7iV6l2
GXhhK1j4bjjgG+jzs17hbpY2XIMDYPEZ4xtVX1ECgYBcCCqFKTAkEQ08cd01OIBe
WojQErNBTDClj6Sjp8kLeDUZANs7uK5nAbvetPyQePig97XiDXXUrWZnWeEZpv6T
Swq7sKTsnvTJdYH0DUJj5ATQHfRCRm1Ws3rL6MFlxvm3HgX4Jk0If9U8EdBuYm42
1xUsXEK9iGaeayAKvVQoTwKBgCrkuLI7uxVB/h0L3ZKo5ewuy03PV/tokQohR3wu
Ugg5Q+9z6+PxObfnFmzCtjX2hjZdrLeqV9T0K3C1o9ilqhnp9BcwsaaRUh18MjPn
sODHCj1cJgA6AJ8qQKpKIMcB21Alz3VPhObUGwwXRQfq0R7tSuhaLGR1BtqXaH7h
v+DRAoGBALzGh/T7DZkY7arHDgHBS41S6mLWSflwwLxs99zOg1lPFLC0z9zMBAgU
g9bKo8kzcH0g6Z3PPsOt9Va0ou9iz89OnWDedCsGR0By3AYzaKjDPQSlatDDvkKY
jwmrIGEHDKwcNm+gI3d/6aRwuy3ZwH7yLkTsoysad3ivhb1879Ff
-----END RSA PRIVATE KEY-----"""

    def get_public_key(self, account):
        return """-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAyK+8fQC9aiDQpTpwpJCyXjnk9SDKkE/Nys3POPcGf64FZeRux70Q
Bh1sdUD6P3yZYH4eJrm913wNZQMgxOlFX1PXxfZYT9h/USOsb61YjA0ZWvT/iwqJ
d3UlVXiNZf++OwFXqkhjS7EqtamIVS6DHA8SYAoJcGw710DiYg9iGtx3iMjADICA
+V6kzvNp/j6DB03Nix9kcu4OTRPq2iPOJwL4Mxwon2S8i8XW7HgQvlZd0sPcgDSh
1pvABjF4gjvEViRXgjg2ejcOwl3QR0zBIOGhuzBPFR5gkaD25ed6mEnhrkBHnG7B
u0LqHrNXjIctnl2F8vT6MqkQ+kQGIUDEuwIDAQAB
-----END RSA PUBLIC KEY-----
"""
    def deploy_contract():
       with open('documentflow.sol') as file:
        contract_source_code =file.read().encode('utf-8')
        compiled_sol = compile_source(contract_source_code) # Compiled source code
        contract_interface = compiled_sol['<stdin>:DocumentFlow']
        # Instantiate and deploy contract
        DocumentFlow =self.w3.eth.contract(abi=contract_interface['abi'], bytecode=contract_interface['bin'])
        # Submit the transaction that deploys the contract
        tx_hash = DocumentFlow.constructor().transact()
        # Wait for the transaction to be mined, and get the transaction receipt
        tx_receipt = self.w3.eth.waitForTransactionReceipt(tx_hash)
        # Create the contract instance with the newly-deployed address
        documentflow = self.w3.eth.contract(
        address=tx_receipt.contractAddress,
        abi=contract_interface['abi'],
)
    def post_file_send_action(self, file_hash, recipient):
        valid_address = self.w3.toChecksumAddress(recipient)
        s=self.documentflow.functions.postFileSendAction(file_hash,valid_address).transact()
        return s.hex()


    def post_password_send_action(self, file_hash, password):
        s=self.documentflow.functions.postPasswordSendAction(file_hash, password).transact()
        return s


    # @return String return file hash
    def get_pending_file(self):
        s=self.documentflow.functions.getPendingFile().call()
        return s #"5a7a49e0f86ea1689b50e6196a946c7aede481eb4cf8df074c3692c18bcb98fe"
    def get_password_send_action(self, file_hash):
        s=self.documentflow.functions.getPasswordSendAction(file_hash).call()
        return s #'hy/cSdET/7IbDfiDZVJSNkTbZJeZJn8PbLGfXmtRz2qUSjYNMt+rF3R+g0E8UOB2E4d2l9CKFBpzjPSIEbYm4QLnwxLKBOX/fQQO4qs7G1KRXSZc9NpxAweBVBh/RztrJgu3bwN2X3vn7iyXJ6GYpZsKQuNh5SWL2eBRQweHDpp4Joy086XHrGysxWVgaaay6/XR/W52iPtnL7AaJzMTFr53QQJ5J8TelGE+Z9e1tL8H24DEt88e2GebwcSd3X9P/RK2AdeB1fCtUe9zQhXHLk16tvflQEbC/sS+J+qerf7TjKm8waDJrouAuzTXKMZkJicvkE5UpJpAyABIgWS+Wg=='

    def post_recieved_file_action(self, file_hash):
        s=self.documentflow.functions.postRecievedFileAction(file_hash).transact()
        return s
    # status: FILE_UPLOADED, FILE_RECIEVED, PASSWORD_SEND, FILE_PROCESSED_SUCCESS, FILE_PROCESSED_FAILED
    def post_read_success_file_action(self, file_hash):
        s=self.documentflow.functions.postReadSuccessFileAction(file_hash).transact()
        return s
    def post_read_failed_file_action(self, file_hash):
        s=self.documentflow.functions.postReadFailedFileAction(file_hash).transact()
        return s
