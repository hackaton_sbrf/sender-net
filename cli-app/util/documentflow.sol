pragma solidity ^0.4.21;
contract DocumentFlow {
     enum Status {FILE_UPLOADED, FILE_RECIEVED, PASSWORD_SEND, FILE_PROCESSED_SUCCESS, FILE_PROCESSED_FAILED }

    struct Snapshot {
        address toRecepient;
        address fromSender;
        Status status;
        string  fileHash;
    }
    Snapshot [] snapshots;
    
    struct FilePassword{
        string  fileHash;
        string password;
        
    }
    FilePassword [] filepasswords;
    
    function compareStrings (string a, string b) internal returns (bool){
       return keccak256(a) == keccak256(b);
   }
    
    function postFileSendAction(string _filehash, address _recepient) public {
	Snapshot memory s;
        s = Snapshot({toRecepient:_recepient, fromSender:msg.sender, status:Status.FILE_UPLOADED, fileHash:_filehash});
	snapshots.push(s);
    }
    
    function getPendingFile () public returns (string){
        uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(snapshots[i].status==Status.FILE_UPLOADED && snapshots[i].toRecepient==msg.sender){
                return snapshots[i].fileHash;
                break;
            }else{
            return "";
		}
        }
    }
    
    function postRecievedFileAction(string _filehash) public{
                uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(compareStrings(snapshots[i].fileHash, _filehash)){
                snapshots[i].status=Status.FILE_RECIEVED;
                break;
            }
        }
        
    }

    function getPendingPassword() public returns (string) {
        uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(snapshots[i].fromSender==msg.sender && snapshots[i].status==Status.FILE_RECIEVED){
                return snapshots[i].fileHash;
                break;
            }else{
            return "";
                }
        }        
        
    }
    
    function postPasswordSendAction(string _filehash, string _password) public {
        FilePassword memory f;
        f=FilePassword({fileHash:_filehash, password:_password});
        filepasswords.push(f);
        uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(compareStrings(snapshots[i].fileHash, _filehash)){
                snapshots[i].status=Status.PASSWORD_SEND;
                break;
            }
        }
        
    }
    
    function getPasswordSendAction(string _filehash) public returns (string){
        uint arrayLength=filepasswords.length;
        for (uint i=0; i<arrayLength; i++) {
            if (compareStrings(filepasswords[i].fileHash, _filehash)){
                return filepasswords[i].password;
            }else{
            return "";
                }
        }
    }
    
    function postReadSuccessFileAction(string _filehash) public {
        uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(compareStrings(snapshots[i].fileHash, _filehash)){
                snapshots[i].status=Status.FILE_PROCESSED_SUCCESS;
            }
        }
    }

    function postReadFailedFileAction(string _filehash) public {
        uint arrayLength=snapshots.length;
        for (uint i=0; i<arrayLength; i++) {
            if(compareStrings(snapshots[i].fileHash, _filehash)){
                snapshots[i].status=Status.FILE_PROCESSED_FAILED;
            }
        }
    }

}
