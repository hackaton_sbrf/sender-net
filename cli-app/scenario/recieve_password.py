from util.ethereum import Ethereum
from util.configuration import Configuration
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_PKCS1_v1_5
from base64 import b64decode,b64encode
from Crypto.Cipher import PKCS1_v1_5 as Cipher_PKCS1_v1_5
import json
from Crypto.Cipher import AES
from Crypto.Hash import SHA
from Crypto import Random
import hashlib


def _decrypt_package(password, package):
    cipher = AES.new(hashlib.sha256(password).digest(), AES.MODE_EAX, b'0123456789abcdef')
    return cipher.decrypt(package).decode("utf-8")

def _decrypt_password(privkey, password):
    dsize = SHA.digest_size
    sentinel = Random.new().read(15 + dsize)
    cipher = Cipher_PKCS1_v1_5.new(RSA.importKey(privkey))
    encrypted_password = cipher.decrypt(b64decode(password), sentinel)
    return encrypted_password

def do_recieve_password_action(file_hash):
    ethereum = Ethereum(Configuration.instance().get_ethereum_node_host(), Configuration.instance().get_ethereum_account())
    encrypted_password = ethereum.get_password_send_action(file_hash)
    if encrypted_password == "":
        print("No password recieve for {}".format(file_hash))
        return

    print("Password recieve for {}".format(file_hash))

    decrypted_password = _decrypt_password(ethereum.get_own_private_key(), encrypted_password)
    file_path = "{}/{}".format(Configuration.instance().get_storage_path(), file_hash)

    with open(file_path) as file:
        data = _decrypt_package(decrypted_password, b64decode(file.read().encode('utf-8')))
        package = json.loads(data)
        source_file_path = "{}/{}".format(Configuration.instance().get_files_path(), package["name"])
        with open(source_file_path, "w") as source_file:
            source_file.write(package["content"])
            print("Decrypt and create file {}".format(source_file_path))
            tx_num = ethereum.post_read_file_action(file_hash)
            print("Send transaction File readed({})".format(tx_num))
