import util.ethereum
import secrets
import string
import json
from Crypto.Cipher import AES
from util.ethereum import Ethereum
from util.configuration import Configuration
import hashlib
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher_PKCS1_v1_5
from base64 import b64decode,b64encode
import os

def _package_file(file_path):
    path, name = os.path.split(file_path)
    with open(file_path, 'r') as file:
        content = file.read()
        return {"hash": hashlib.sha256(content.encode('utf-8')).hexdigest(), "package": {"name": name, "content": content}}


def _gen_password():
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for i in range(20))

def _encrypt_password(pubkey, password):
    cipher = Cipher_PKCS1_v1_5.new(RSA.importKey(pubkey))
    encrypted_password = cipher.encrypt(password.encode("utf-8"))
    return b64encode(encrypted_password).decode("utf-8")

def _encrypt_package(password, package):
    cipher = AES.new(hashlib.sha256(password.encode('utf-8')).digest(), AES.MODE_EAX, b'0123456789abcdef')
    return b64encode(cipher.encrypt(json.dumps(package).encode("utf8"))).decode("utf-8")

def do_send_file_action(file_path, recipient):
    ethereum = Ethereum(Configuration.instance().get_ethereum_node_host(), Configuration.instance().get_ethereum_account())
    password = _gen_password()
    data = _package_file(file_path)
    encrypted_password = _encrypt_password(ethereum.get_public_key(recipient), password)

    password_file_path = "{}/{}".format(Configuration.instance().get_temporary_path(), data["hash"])
    with open(password_file_path, "w") as password_file:
        password_file.write(encrypted_password)

    external_package_path = "{}/{}".format(Configuration.instance().get_mount_path(), data["hash"])
    with open(external_package_path, "w") as external_package:
        external_package.write(_encrypt_package(password, data["package"]))
    print("Send file {} to {}".format(data["hash"], recipient))
    tx_num = ethereum.post_file_send_action(data["hash"], recipient)
    print("Send transaction Sent File({})".format(tx_num))
