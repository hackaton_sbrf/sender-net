from util.ethereum import Ethereum
from util.configuration import Configuration


def do_send_password_action(file_hash):
    ethereum = Ethereum(Configuration.instance().get_ethereum_node_host(), Configuration.instance().get_ethereum_account())
    password_file_path = "{}/{}".format(Configuration.instance().get_temporary_path(), file_hash)
    with open(password_file_path) as password_file:
        print("Send password for {}".format(file_hash))
        tx_num = ethereum.post_password_send_action(file_hash, password_file.read())
        print("Send transaction Send Password({})".format(tx_num))
