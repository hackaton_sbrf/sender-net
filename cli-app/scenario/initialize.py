from util.configuration import Configuration
import pathlib

def do_initialize_action():
    configuration = Configuration.instance()

    if configuration.get_files_path() != "":
        pathlib.Path(configuration.get_files_path()).mkdir(parents=True, exist_ok=True)
        print("Directory {} was created.".format(configuration.get_files_path()))

    if configuration.get_temporary_path() != "":
        pathlib.Path(configuration.get_temporary_path()).mkdir(parents=True, exist_ok=True)
        print("Directory {} was created.".format(configuration.get_temporary_path()))

    if configuration.get_storage_path() != "":
        pathlib.Path(configuration.get_storage_path()).mkdir(parents=True, exist_ok=True)
        print("Directory {} was created.".format(configuration.get_storage_path()))
