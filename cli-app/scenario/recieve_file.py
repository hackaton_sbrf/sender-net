from util.ethereum import Ethereum
from util.configuration import Configuration
import shutil

def do_recieve_file_action():
    ethereum = Ethereum(Configuration.instance().get_ethereum_node_host(), Configuration.instance().get_ethereum_account())
    file_hash = ethereum.get_pending_file()

    if file_hash == "":
        print("No files to recieve")
        return

    from_path = "{}/{}".format(Configuration.instance().get_mount_path(), file_hash)
    to_path = "{}/{}".format(Configuration.instance().get_storage_path(), file_hash)
    shutil.copy2(from_path, to_path)
    print("Recieve file {}".format(file_hash))
    tx_num = ethereum.post_recieved_file_action(file_hash)
    print("Send transaction File Recieved({})".format(tx_num))
