import click
from scenario.send_file import do_send_file_action
from scenario.recieve_file import do_recieve_file_action
from scenario.send_password import do_send_password_action
from scenario.recieve_password import do_recieve_password_action
from scenario.initialize import do_initialize_action

@click.group()
def main():
    pass

@main.command()
@click.argument("file_path")
@click.argument("recipient")
def send_file(file_path, recipient):
    """Send file"""
    do_send_file_action(file_path, recipient)

@main.command()
def recieve_file():
    """Recieve file"""
    do_recieve_file_action()

@main.command()
@click.argument("file_hash")
def send_password(file_hash):
    """Send password"""
    do_send_password_action(file_hash)

@main.command()
@click.argument("file_hash")
def recieve_password(file_hash):
    """Recieve password"""
    do_recieve_password_action(file_hash)

@main.command()
def initialize():
    """Initialize directories"""
    do_initialize_action()

if __name__ == '__main__':
    main()
